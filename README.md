# Documentation for Rhei Platoform

To build the docs, you'll need Python 3, and then install:

    $ pip install sphinx sphinx_rtd_theme recommonmark

Once done with that, build the thingie:

    $ cd src
    $ make html

If there's a folder `web-site` parallel to where `rhei-documentation` is and it contains subfolder `public`, `make html` will copy built docs into `web-site/public/learn`.
