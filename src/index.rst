
Rhei Platform documentation
===========================

Rhei Platform is an open-source system that implements Flow-Based Programming concepts.


.. image:: images/rdyflow.svg
    :align: center

|

Using Rhei, you would code your application logic by connecting ``component nodes`` into ``data-flow networks``.

Then, you would feed your data into the network in a form of ``information packets``.
Information packets flow through ``connections`` between networked component nodes.

Each component node transforms the data in some way or produces new data based on what it consumed at its inputs.

The result of your network's (your application's) work are those information packets that leave the network.

Rhei Platform is designed to be easily embedded into existing applications. You can:

- Embed Rhei into your application by implementing parts of your app logic in FBP
- Embed Rhei into your application and expose it as a scripting environment to your end users
- Create standalone services or CLI applications
- Embed Rhei into firmware of your microcontroller based IoT boards


.. toctree::
    :maxdepth: 4
    :caption: Contents:

    content/quick_dive
    content/fbp_in_nutshel
    content/rhei_basic_concepts
    content/embedding_rhei
    content/standalone_rhei
    content/building_for_iot
    content/writing_components
    content/qas
    content/bits_n_pieces
..    indexes_and_tables
