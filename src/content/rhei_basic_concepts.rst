Rhei: Basic concepts
====================

When using FBP, in general, it can be in one of two broad roles:

- as an application developer
- as a component developer

Think of these two roles as equivalents of someone who codes applications using,
for example, PHP (application developer) on one hand, and someone who codes
libraries or frameworks for PHP (component developer) on the other.

To continue analogy, when working with FBP, most people would spend their time in
application developer role, equivalent to coding your app in PHP, and some would devote
their time being component developers, equivalent to writing libraries and frameworks for PHP.

In Flow-Based Programming, these two roles work on different levels of abstraction:

- **application developer** works on creating networks of components and is more concerned with how those components will work together
- **component developer** works on creating a specific components and is focused on the component itself, defining its inputs and outputs and coding its logic



Flow-Based Programming application developer role
-------------------------------------------------

Most of the time, as an application developer you would either:

- design your netowrks on paper (real or digital) and encode them for Rhei as ``bpnet`` text files, or
- use GUI editor, like ``rdyFlow``,  to design and encode your networks for Rhei CLI tools to process

Here is a sample of FBP network encoded as text in a Rhei ``bpnet`` file::

    ;
    ; buffers
    ;

    { value_A } <-
        i32 42
    EOB <-
    { value_B } <-
        i32 55
    EOB <-
    { output_buffer } <- extern

    ;
    ; contexts
    ;
    [ adder ] <-- tests.math.adder
    < sendValueOnce > <-- tests.values.senders
    < outputFn > <-- tests.system.printline

    ;
    ; connections
    ;
    < sendValueOnce, value_A > --> 0 : [adder]
    < sendValueOnce, value_B > --> 1 : [adder]
    [adder] --> < outputFn, output_buffer >

Here is a sample of using ``rdyFlow`` GUI to create a network for Rhei:

.. raw:: html

    <video width="100%" controls>
        <source src="../_static/rdyFlow.mp4" type="video/mp4">
        Your browser does not support the video tag.
    </video> 

|

Designing a network in Rhei consists of the following activities:

- adding component instances (a.k.a. network nodes) to the network
- connecting a node output(s) with other node input(s)
- defining inputs on the network level
- defining outputs on the network level

Once the design is done, you would use Rhei Tools, either through GUI or in command-line,
to compile your network(s).

Result of the network(s) compilation might be either a standalone service/app executable
or one or more source code or binary files to be included into already existing application.


Flow-Based Programming component developer role
------------------------------------------------

As a component devloper, you would use some sort of text-oriented IDE, like Visual Studio Code for example,
to write your component source code.

Here is an example of a very simple component that adds two numbers::

    input A as int32
    input B as int32
    output OUT as int32

    packet left as int32
    packet right as int32
    packet result as int32

    setup {}

    loop {
        receive left from A
        receive right from B
        result := left + right
        send result to OUT
    }

Once you write your component code, you would then use Rhei Tools compiler to produce ``bpasm`` assembler file
for it. This file is used by Rhei network compiler to compile networks for application developers.

You would also have to write some description/documentation for you component as well as create
``component definition JSON file`` to be used by GUI tools like **rdyFlow**.

Here is a sample ``components.json`` file which contains description for just one component, our simple adder::

    {
        "Add": {
            "source": "adder",
            "label": "A + B",
            "icon": "icons/math_binary.svg",
            "type": "component",
            "setups": [],
            "inputs": [
                {
                    "name": "LEFT",
                    "pinType": "default",
                    "pinColor": "blue"
                },
                {
                    "name": "RIGHT",
                    "pinType": "default",
                    "pinColor": "blue"
                }
            ],
            "outputs": [
                {
                    "name": "OUT",
                    "pinType": "default",
                    "pinColor": "orange"
                }
            ]
        }
    }


Under the hood
--------------

Rhei Platform is designed from the start to be used as an embeddable module,
for example similar how `Lua <https://www.lua.org/about.html>`_ is designed.

That doesn't mean you can't create standalone applications or services with it:
Rhei comes with templates to build simple CLI applications or REST/raw socket based services,
and you can, of course, provide your own *runners* for Rhei VM.

Rhei Platform consists of few main parts:

- Rhei VM
- Rhei Tools
- rdyFlow GUI

Rhei VM
~~~~~~~

**Rhei VM** is the core of Rhei platform and is tha main thing that differentiates Rhei
from other FBP implementations. Main part of the VM is **interpreter** which is coded in C,
following ANSI/ISO C99 standard, and is designed to be easy to use and to embed into other applications.

Besides the interpreter, Rhei VM contains single-threaded cooperative and multi-threaded *VM scheduler*
implementations, *data-access API*, dynamic *network builder* API, binary component and network *loaders*,
and provides facilities and API for creating and working with *Native Function Interface Tables* (a.k.a. NFITs).
NFIT concept and API is the point of integration between Rhei and third-party (a.k.a. "your existing application")
code.

Rhei Tools
~~~~~~~~~~

**Rhei Tools** consists of three main parts:

- ``bpsrc`` component source compiler
- ``bpasm`` component assembler specification compiler
- ``bpnet`` network-to-source and network-to-binary compiler

You can use one of these CLI tools to compile and build your Rhei networks or you can create your own
tools by linking to and calling relevant methods in Rhei Tools libraries.

rdyFlow GUI
~~~~~~~~~~~

**rdyFlow GUI** is HTML/Javascript based graphical interface you can use to design your networks and then
compile and build them. Of course, *rdyFlow* uses Rhei Tools to do its job under the hood. *rdyFlow* can
be deployed on-line and it can then either produce input for you to download and use with Rhei Tools on
you local machine, or it can do the work on the server.

Out-of-the box, Rhei Platform provides desktop version of *rdyFlow* which runs on CEF via ``cefpython3``
module and uses ``Python 3`` in the background to do all the work via Rhei Tools.


Essential characteristics of FBP in Rhei
----------------------------------------

As previously said, main concepts of Flow-Based Programming are:

- Components/Nodes
- Connections
- Networks
- Information Packets (data)

Components / Nodes
~~~~~~~~~~~~~~~~~~

Rhei works with **component definitions**, also called **components**. These are essentially templates
used to create **nodes** in a network. As expected, you can have more than one instance of a component in
a network. For example, a component might be called ``Adder`` and you could use this one component
(the definition) to create multiple nodes in your network, i.e. ``summator_1``, ``summator_2``, ``summator_3``,
etc. depending on what processing logic you're implementing with that network.

Within Rhei VM, network nodes are also simply called **contexts**, since they provide a VM context within
which a specific component code is executed.

Components usually have input and/or output ports, named with respect to the component. This means that
an **input port** is used by the component code to **receive** information packets in, and an **output port** is
used by the component code to **send** information packets out.

.. admonition::
    To summarize:

    - *Components* are *templates* used to create *network nodes*
    - A component has:

        - *input* ports
        - *output* ports


Connections
~~~~~~~~~~~

As said, components usually have input and/or output ports. You would connect these ports to other component's
ports to create **connections** between two component instances (nodes).

**Connection** represents a means of transporting **information packets**, or data, between nodes.
Every connection has a **capacity** which represents a number of information packets the connection
can hold before an attempt to write to the connection is blocked. If you don't specify otherwise,
default capacity for a connection is ``1``.

Every connection has a **source port** and a **sink port** associated with it and named with respect to
the connection, which means the source port is *source* of information packets for the connection
and the sink port is *sink* of information packet for the connection.

You can only use one component's *output* as connection's *source* and other component's *input* as
connection's *sink*.

All connections are **uni-directional only**. This means you can only place an information packet
to a connection through **source** port (``send``) and read an information packet from it
through **sink** port (``receive``).

.. admonition::
    To summarize:

    - *Connections* carry *information packets* between input/output ports of network nodes
    - Connections are *uni-directional*
    - A connection has:

        - *source* port
        - *capacity*
        - *sink* port

Netowrks
~~~~~~~~

Once you create one or more *nodes* and connect various ports using *connections*, you've got yourself
a **network**.

A network has to have **at least one node** and can have **zero or more connections**.

Besides nodes and connection, you may or may not define **network inputs and/or outputs**. If you do define
inputs and/or outputs for your network this usually means you have designed a **sub-network**. There's nothing
particulary special about sub-networks: they are simply convenient ways to organize you application into
smaller, and possibly reusable, chunks.

There is one caveat with network inputs/outputs: they **have to be connected** to something before
the network is ran by VM.

.. admonition::
    To summarize:

    - *Networks* are collections of component *nodes* inter-connected by *connections*
    - A network *has to have* at least *one node*
    - A network can have *zero or more connections*
    - A network may or may not have one or more:

        - *network inputs*
        - *network outputs*


Information Packets
~~~~~~~~~~~~~~~~~~~

Once you are designed, compiled, built and deployed your network, it is time for it to process some data.

Data in Flow-Based Programming is processed in chunks called **information packets** or **IP** s.

When a component receives data on an input port or sends data to an output port, it does it one information packet
at a time. A connection between components can hold some data that is being transferred from one port to another
and this data is information packets. Connection capacity is measured in maximum number of information packets
it can contain at one time.

An information packet can be *owned* by only **one single owner** at **any one time**. This owner is either a component
or a connection. Components transform data contained within information packets, and connection simply transfer them
from one component to the other.

Essentially, an information packet is simply a data buffer with designated owner. In Rhei, IPs are created on top of
binary (byte) buffers. IP data buffers have **no intrinsic structure** and it is up to a component to interpret
the data in IP's binary buffer.

.. admonition::
    To summarize:

    - Components process and connections transport data in chunks called *information packets*
    - An information packet has *only one* owner at any one time
    - Rhei implements IPs on top of *byte buffers* which have *no intrinsic structure*


VM Interpreter
~~~~~~~~~~~~~~

There are few essential traits of Rhei VM you need to keep in mind when designing your apps and components.

These traits are not imposed by some VM implementation quirk or specificity, they are actually
implementations of so called *classical* Flow-Based Programming specification.

Here they are, in no particular order:

|

Rhei Virtual Machine Interpreter for FBP is implemented in two variants:

- single-threaded cooperative
- multi-threaded preemptive

You don't have to do anything special when designing your application networks or components with respect to
either of these variants. Any Rhei network or component will transparently run on either single- or multi-threaded
Rhei VM.

However, this means that VM executes the code in active network nodes:

- one node after another, in no particular order, in cooperative mode
- in parallel, governed by underlying OS thread scheduler, in multi-threaded mode

When component performs read on input port (receive from port), code execution will block
if connection is empty, and continue once IP is available for read.

When component performes write on output port (send to port), code execution will block
if connection is full, and continue once space is available for IP to be written to.

If a component code closes **input** port, connection to that port will be **terminated**,
any IPs in the connection FIFO will be discarded, and connection's **source** port will be closed.

If a component code closes **output** port, connection to that port will be **terminated**
but **only** when it's FIFO becomes **empty**. Once connection is terminated,
its **sink** port will be closed.

Sending/receiving on closed ports will result in error condition for a component and its
execution will be suspended.

Once **all ports** of a node have been closed, VM will **terminate** execution of that node.
The node will not be scheduled for execution again by cooperative scheduler. In multi-threaded
mode, its thread will be killed.

Inside Rhei VM, a network node can be in one of these states (and a few more, but relevant only internally):

- ``running``
- ``suspended``
- ``terminated``

Once **there are no** nodes in ``running`` state, network execution will be stopped and main VM
scheduler loop will be terminated. You can **continue** to run a network that has no ``running`` but
has at least one ``suspended`` node. This usually means that a component is waiting for IP to become
available on one of its input ports. If **all** nodes are ``terminated`` you can submit the network to VM
for a run but VM will not do anything.

.. admonition::
    To summarize:

    - Component code in nodes is executed **in parallel**
    - A **port read** will **block** node execution while connection is **empty**
    - A **port write** will **block** node execution while connection is **full**
    - Closing **input** port will **terminate** associated connection, drop all IPs in it and **close** output port on the other side
    - Closing **output** port will **terminate** associated connection **once it is empty**, and it will **close** input port on the other side
    - Component will end ``suspended`` in **error** condition if it sends or receives on **closed** port
    - Once **all ports** on a node have been **closed**, the node execution is **terminated**
    - If all nodes in a network are ``terminated``, the network is done with its work
    - If there's at least one ``suspended`` or ``running`` node in a network, it may still have work to do

