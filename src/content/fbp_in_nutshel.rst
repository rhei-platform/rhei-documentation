Flow-Based Programming in a nutshell
====================================

.. tip::
    For detailed dive into Flow-Based Programming, visit https://jpaulm.github.io/fbp/index.html.

Here is Flow-Based Programming in a nutshell, in the words of its inventor J. Paul Morrison (citations from https://www.jpaulmorrison.com/fbp/noflo.html):


**ABOUT GETTING STARTED**:

.. epigraph::

    FBP supports data processing applications (business or scientific),
    typically long-running and high volume, and, as we have shown,
    involves a way of thinking (the new "paradigm") that is fundamentally
    different from that of conventional programming. This paradigm is actually
    more similar to engineering than to conventional programming, and,
    not surprisingly, involves a period of what might be called "apprenticeship",
    during which the practitioner is getting comfortable using its concepts.


**ABOUT NETWORK / APPLICATION**:

.. epigraph::

    An application built using FBP may be thought of as a "data processing factory":
    a network of independent "machines", communicating by means of conveyor belts,
    across which travel structured chunks of data, which are modified by successive "machines"
    until they are output to files or discarded. The various "machines" run in parallel,
    or interleaved, as determined by the number of processors in the machine.


**ABOUT DATA**:

.. epigraph::

    in FBP, data is managed in packets (IPs), which have a well-defined lifetime,
    from creation to destruction, and can only be owned by one process at a time,
    or be in transit between processes - just like real-life objects.


**ABOUT NODE / COMPONENT**:

.. epigraph::

    Granted each FBP process is a von Neumann program, but it runs independently of
    all other processes, and so tends to be quite simple internally. Almost all of
    the data that an FBP process deals with is held in "information packets" (IPs) or in
    method local storage. Unlike in conventional programming, the programmer does not have
    to worry about controlling the exact sequence of events - all s/he needs to
    concentrate on is the transformations that apply to the data to convert the
    original inputs to the desired output.

