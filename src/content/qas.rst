QAs
===

What is the status of the project? How far is this?
---------------------------------------------------

asdasd


There are other FBP implementations out there, what's new with this one?
------------------------------------------------------------------------

Existing ones are libraries/frameworks for specific programming languages
(Java, Ruby, C++, etc.) and sometimes platforms (i.e. some need NodeJS
to run JavaScript code that the framework is implemented in)


How is this FBP different from any other visual tools out there (a/v pipline builders, 3dmax MCG, UnrealEngine blueprints etc.)?
--------------------------------------------------------------------------------------------------------------------------------

- control flow is present, explicit (mcg, blueprints) or implicit (a/v pipeline)
- code blocks / scratch and others...

Main difference:

All these tools have specifics you have to learn, even FBP-inspired implementations.
"Classical FBP" is general-purpose programming concept that has one set of rules,
i.e. it is equivalent of one programming language: you learn basic concepts and 
those are the same wherever you happen to work with software that is either 
FBP-based application or uses FBP as embedded tool: in audio/video pipeline tools, 
game logic programming, data processing, application/service logic, 
microcontroller code, programmable logic controllers, etc.

