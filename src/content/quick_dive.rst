Quick dive
==========

Follow these steps to get a quick glimpse of how it is to work with Rhei.


Requirements
------------

Before you start, these tools need to be installed on your workstation:

- Python 3.7
- *one* of these compilers:

    - GCC 7.4
    - clang 10.0

- git 2.17
- nodejs with npm, any nodejs version 10+ should work

These are the versions used at the time of this document writing, newer versions may or may not work.

Once you installed Python, git and one of the C compilers on your workstation, you'll need to install
a few Python packages. We will use Python ``pip`` package manager here, you can use whichever one you
like as long as you end up with correct packages installed.

Python packages to install:

- scons (3.1.1)
- pytest (5.2.2)
- pyinstaller (3.5)
- cefpython3 (66.0)

The package version is given in parenthesis, as of this document writing, and again: newer versions may
or may not work.

To install packages run::

    pip install scons pytest pyinstaller cefpython3

With all the tools and Python packages installed, it's time to get the show on the road!


Get the sources
---------------

For this quick dive we will need repositories for following Rhei bits and pieces:

- Rhei VM (the interpreter)
- Rhei Tools (compiler, assembler, netbuilder)
- rdyFlow (GUI)
- component samples

Create a working folder for us first::

    $ mkdir rhei-quick-dive

and then clone each of the repositories::

    $ cd rhei-quick-dive
    $ git clone https://gitlab.com/rhei-platform/rhei-vm.git
    $ git clone https://gitlab.com/rhei-platform/rhei-tools.git
    $ git clone https://gitlab.com/rhei-platform/rdyflow.git
    $ git clone https://gitlab.com/rhei-platform/sample-components.git


Build the stuff
---------------

We have two main parts to build:

- Rhei VM library
- Rhei CLI tools

We won't be building and packaging the desktop version of rdyFlow GUI, we will just run it directly using Python instead.


Build the VM
~~~~~~~~~~~~

First, let's build Rhei VM::

    $ cd rhei-vm
    $ scons --vm

You should end up with new ``build`` folder in ``rhei-vm`` with VM library in there::

    $ ls build
    libbpsvm.a  vm_runner

Now build and run VM tests::

    $ scons --tests --run

This step will take a bit longer since C++ needs more time to do its work. Once the build finishes
you should se something similar to::

    ...
    ...
    ===============================================================================
    All tests passed (939 assertions in 44 test cases)

    scons: done building targets.


Build the tools
~~~~~~~~~~~~~~~

Now it's time to build Rhei CLI tools. Change the working folder to where we cloned Rhei Tools repository::

    $ cd ../rhei-tools

First, we will invoke ``bpasm`` and ``bpnet`` via Python, just to make sure everything works ok::

    $ python assembler/src/bpasm.py
    usage: bpasm.py [-h] [-of OUTPUT_FOLDER] file_name
    bpasm.py: error: the following arguments are required: file_name

    $ python assembler/src/bpnet.py
    usage: bpnet.py [-h] [-cp COMPONENT_PATH] [-of OUTPUT_FOLDER] file_name
    bpnet.py: error: the following arguments are required: file_name

and run the tests::

    $ python assembler/tests/run.py
    ...
    ...
    Ran 63 tests in 0.152s

    OK

Now, we'll build ``bpasm`` and ``bpnet`` binaries::

    $ scons --asm
    ...
    scons: done building targets.

    $ scons --net
    ...
    scons: done building targets.

Once the builds are done you should have a brand sparkling new Rhei CLI Tools binaries in ``build`` folder::

    $ ls build
    bpasm  bpnet  pyinsttmp

Execute each to make sure there are no issues::

    $ ./build/bpasm
    usage: bpasm.py [-h] [-of OUTPUT_FOLDER] file_name
    bpasm.py: error: the following arguments are required: file_name

    $ ./build/bpnet
    usage: bpnet.py [-h] [-cp COMPONENT_PATH] [-of OUTPUT_FOLDER] file_name
    bpnet.py: error: the following arguments are required: file_name

Yay! It's alive!

Now, we are ready to create some simple nets using a text editor and CLI.



Build the GUI
~~~~~~~~~~~~~

To run GUI, you'll need Python and ``cefpython3`` module installed. You'll also need nodejs and npm. Once that is done, we can build the GUI::

    $ cd ../rdyflow
    $ ./build_cef.sh

This simply builds and bundles javascript source and copies what's needed into ``build`` folder. Now we can run the GUI::

    $ ./serve.sh
    Starting up http-server, serving .
    Available on:
    http://127.0.0.1:9797
    http://192.168.0.15:9797
    Hit CTRL-C to stop the server
    ...etc...

This should open a browser window with rdyFlow running in it. If it doesn't, 
open a browser window and navigate to whatever is printed under "Available on:", i.e. navigate to ``127.0.0.1:9797``.

You should see something like this:

.. image:: ../images/rdyFlow-Rhei_Platform_GUI.png
    :align: center

Now, open *Value Emitters* and drag-and-drop two **value**'s:

.. image:: ../images/rdyFlow-step-1.png
    :align: center

Next, from under *Math* drag-and-drop **A+B**:

.. image:: ../images/rdyFlow-step-2.png
    :align: center

And finally, from *System* drag-and-drop **syste.out.print** to the net canvas:

.. image:: ../images/rdyFlow-step-3.png
    :align: center

and connect inputs and outputs:

.. image:: ../images/rdyFlow-step-4.png
    :align: center

|

Light-grey boxes represent **native functions** and dark-grey represent **components**.

We need to set up **configuration buffers** for native functions before we can go further.
Each native function uses configuration buffers in a way specific to it, therefore, in general,
config buffers have different formats and content.

The **value** native function will take whatever we put as a content of its config buffer, it will
create an IP and send it to its ``OUT`` port. **A+B** sample component expects 32-bit integer numbers
on its ``LEFT`` and ``RIGHT`` inputs so that's what we will put in **value** config buffers, to be sent to **A+B** component node.

Hover mouse over the top **value** and then click little *i* icon:

.. image:: ../images/rdyFlow-step-5.png
    :align: center

"Component Node Options" dialog will pop-up:

.. image:: ../images/rdyFlow-step-6.png
    :align: center

Enter following into the "Config Buffer" field::

    i32 42

and click "Change it" button.

Do the same for bottom **value** but use number ``55`` instead of ``42``.

In the same manner, click *i* icon on **syste.out.print** node and enter the following
as a value for it's config buffer::

    cs "Result is %i\n

That config buffer contains a string which **system.out.print** will use to print out data from
an IP it received on its ``IN`` port.

Now that's done, click a little boxy icon in the top-right corner, which runs build:

.. image:: ../images/rdyFlow-step-7.png
    :align: center

``rdyFlow`` will convert the diagram you created into a ``bpnet`` source and then it will open a dialog that contains the source code of the network you created:

.. image:: ../images/rdyFlow-step-8.png
    :align: center

We will need content of the "Source" text area for our next step: building and running the network
we just designed.


Make and run a simple network
------------------------------

We'll now create a sub-folder in the ``rhei-quick-dive`` folder::

    $ cd ..
    $ pwd
    /home/user/rhei-quick-dive
    $ mkdir work
    $ cd work
    $ pwd
    /home/user/rhei-quick-dive/work

We'll do our work in this folder.

First, we are going to create a simple network with just one component: the one that adds two integers.

Create a file named ``sample.bpnet`` in our ``work`` folder and paste the content of "Source"
text area from build dialog popup from previous section into it. The content should look like this::

    ; buffers
    { buffer_0 }<--
    i32 42
    EOB <--
    { buffer_1 }<--
    i32 55
    EOB <--
    { buffer_3 }<--
    cs "Result is %i\n
    EOB <--
    ; contexts
    < sendValueOnce > <-- values.senders 
    [ adder ] <-- math.adder 
    < stdoutPrint > <-- system.out 
    ; connections
    < sendValueOnce , buffer_0 > --> 0 : [ adder ] 
    < sendValueOnce , buffer_1 > --> 1 : [ adder ] 
    [ adder ] : 0 --> < stdoutPrint , buffer_3 > 

Our work folder now has just two files in it::

    $ ls
    sample.bpnet

Let's invoke ``bpnet`` tool on our network definiton file::

    $ ../rhei-tools/build/bpnet \
        -cp ../sample-components/tests \
        -of output \
        --simple-cli \
        sample.bpnet

Once ``bpnet`` is done, we sould have a new folder called ``output`` created in our work folder::

    $ ls -1 output/
    adder.h
    out.c
    sample.Makefile
    sample.SConstruct
    sample_main.c
    sample_network.c
    senders.c

Now we need to build an executable using these generated files. For just this purpose, ``bpnet`` has created two files:

- sample.Makefile
- sample.SConstruct

The first one is a **Makefile** you can use to build the executable by issuing::

    $ make -f sample.Makefile

The other one is **SConstruct** file used by ``scons`` and we will use that one. It doesn't matter which one you use,
the result will be the same.

Let's build our executable::

    $ cd output
    $ scons -f sample.SConstruct
    ...
    scons: done building targets.

Now we should have executable named ``sample_main`` in the ``output`` folder::

    $ ls -l
    -rw-r--r--  1 user  user   2217 1 Jan 00:00 adder.h
    -rw-r--r--  1 user  user   1291 1 Jan 00:00 out.c
    -rw-r--r--  1 user  user   1316 1 Jan 00:00 out.o
    -rw-r--r--  1 user  user   1238 1 Jan 00:00 sample.Makefile
    -rw-r--r--  1 user  user   1283 1 Jan 00:00 sample.SConstruct
    -rwxr-xr-x  1 user  user  94836 1 Jan 00:00 sample_main
    -rw-r--r--  1 user  user    791 1 Jan 00:00 sample_main.c
    -rw-r--r--  1 user  user    800 1 Jan 00:00 sample_main.o
    -rw-r--r--  1 user  user   2297 1 Jan 00:00 sample_network.c
    -rw-r--r--  1 user  user   2316 1 Jan 00:00 sample_network.o
    -rw-r--r--  1 user  user   1179 1 Jan 00:00 senders.c
    -rw-r--r--  1 user  user   1288 1 Jan 00:00 senders.o

Let's run it::

    $ ./sample_main
    Result is 97

Yay! 42 + 55 is indeed 97. It works!

Now change one or both values in config buffers (``buffer_0`` and ``buffer_1``) in our network,
let's say change ``42`` to ``-42``, and run ``bpnet`` and ``scons`` again::

    $ cd ..
    $ rm -rf output/*
    $ ../rhei-tools/build/bpnet \
        -cp ../sample-components/tests \
        -of output \
        --simple-cli \
        sample.bpnet
    $ cd output
    $ scons -f sample.SConstruct
    $ ./sample_main
    Result is 13

Hooray! (I know, I'm overenthusiastic sometimes, what can I do, sue me... :) ) 55 + (-42) is indeed 13.

