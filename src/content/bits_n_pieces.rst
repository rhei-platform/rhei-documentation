Bits'n'pieces
=============


use cases
---------

- integrate into existing application/codebase:

    - include C source into your C/C++ application
    - wrap C-code via native interface:

        - Java: via JNI
        - Python: using Cython
        - JavaScript: through Emscripten
        - Ruby: as an extension, using ffi gem, via mkmf
        - PHP: phpize, php-cpp
        - Lua: um, do I embed Rhei into Lua or the other way around? 😁
        - etc. and so on...

    - build as a service and send/receive information packets via:

        - raw sockets
        - REST endpoints


- build standalone applications or services:

    - create main.c (or cpp) and run Rhei VM, similar to the way Lua CLI is ran
    - use provided REST service or raw socket template and let Rhei build the app

        - optionally, Dockerize it


- deploy to microcontroller based boards and IoT devices:

    - 32-bit Rhei VM (with limited features, of course) for ARM-based microcontrollers (NXP LPC, STM32, AVR32)
    - startup code for different microcontrollers to run VM and your application FBP network



Features
--------

- embeddable by design
- VM to run networks
- native parallelism
- native distributed execution

- create Docker service
- native/legacy code interface
- run VM on microcontrollers
- cooperative and multi-threaded VM versions

- visual network editor

    - to build network definitions to be passed to network compiler
    - syntax highlighting for assembly and bpscript source files for Visual Studio Code

Stuff
~~~~~

- Run VM when data for input functions is ready

    - VM runs input functions
    - input functions package your data into IPs and send them to connections
    - VM runs runnable nodes inside your FBP network, which in turn processes the data from IPs
    - VM runs output functions
    - output functions pick up IPs from connections and package processing results into your structures

- Your code takes the result data and moves on
